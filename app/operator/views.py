from flask import Blueprint, request
from werkzeug.security import generate_password_hash

from app.models import *
from app.helpers import json_response, validate_phone, validate_password
from app.operator.helpers import delivery_list, positions_list


operator = Blueprint('operator', __name__)


@operator.route('/register', methods=['POST'])
def register():
    coordinates = Coordinates.query.filter_by(latitude="53.9", longitude="27.56667").first()

    if not coordinates:
        coordinates = Coordinates(latitude="53.9", longitude="27.56667")
        db.session.add(coordinates)

    try:
        db.session.commit()

    except:
        return json_response(
            "fail",
            "Database error",
            500
        )

    if not (request.is_json
            and 'name' in request.json
            and 'login' in request.json
            and 'phone' in request.json
            and 'password' in request.json):
        return json_response(
            "fail",
            "Request contains no JSON body or required field",
            400
        )

    if not validate_phone(request.json['phone']):
        return json_response(
            'fail',
            'Specified phone number don\'t satisfy the requirements',
            400
        )

    if not validate_password(request.json['password']):
        return json_response(
            'fail',
            'Specified password doesn\'t satisfy the requirements',
            400
        )

    client_role = Role.query.filter_by(name="Driver").first()

    trinity = generate_password_hash(request.json['password'],
                                     method='pbkdf2:sha256',
                                     salt_length=8)
    salt, password_hash = trinity.split('$')[1:]

    authdata = Authdata(login=request.json['login'], authhash=password_hash,
                        salt=salt, role=client_role)

    driver = Driver(name=request.json['name'], phone=request.json['phone'],
                    authdata=authdata, coordinates_id=coordinates.rowid)

    db.session.add(driver)

    try:
        db.session.commit()
        return json_response(
            "success",
            "Driver successfully registered",
            200
        )

    except:
        return json_response(
            "fail",
            "Database error",
            500
        )


@operator.route('/list_deliveries', methods=('GET',))
def list_orders():
    try:
        deliveries = Delivery.query.all()

    except:
        return json_response(
            "fail",
            "Database error",
            500
        )

    return delivery_list(deliveries)


@operator.route('/map', methods=('GET',))
def get_positions():
    try:
        drivers = Driver.query.all()

    except:
        return json_response(
            "fail",
            "Database error",
            500
        )

    return positions_list(drivers)


@operator.route('/assign_driver', methods=('POST',))
def assign_driver():
    if not (request.is_json
            and 'driver_id' in request.json
            and 'parcel_id' in request.json):

        return json_response(
            "fail",
            "Request contains no JSON body or required field",
            400
        )

    driver = Driver.query.filter(
        Driver.rowid == request.json['driver_id']
    ).one_or_none()

    if not driver:
        return json_response(
            "fail",
            "No driver with such id found",
            400
        )

    parcel = Parcel.query.filter(
        Parcel.rowid == request.json['parcel_id']
    ).one_or_none()

    if not parcel:
        return json_response(
            "fail",
            "No parcel with such id found",
            400
        )

    to_deliver = Status.query.filter_by(status="TO_DELIVER").first()

    delivery = Delivery(driver=driver,
                        parcel=parcel,
                        report=Report(report=""),
                        status=to_deliver)
    db.session.add(delivery)

    try:
        db.session.commit()
    except:
        return json_response(
            "fail",
            "Database error",
            500
        )

    return json_response(
        "success",
        "Parcel with ID {} assigned to driver with ID {}".format(
            parcel.rowid,
            driver.rowid
        ),
        200
    )

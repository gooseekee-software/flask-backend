import json

from flask import make_response, jsonify


def delivery_list(deliveries):
    json_deliveries = [
        {
            "parcel_id": delivery.parcel.rowid,
            "driver_id": delivery.driver.rowid,
            "delivery_address": delivery.parcel.delivery_address,
            "pickup_address": delivery.parcel.pickup_address,
            "delivery_coordinates":
            {
                "latitude": delivery.parcel.delivery_coordinates.latitude,
                "longitude": delivery.parcel.delivery_coordinates.longitude
            },
            "pickup_coordinates":
            {
                "latitude": delivery.parcel.pickup_coordinates.latitude,
                "longitude": delivery.parcel.pickup_coordinates.longitude
            },
            "status": delivery.status.status
        }
        for delivery in deliveries
    ]

    return make_response(jsonify(
        {
            "status": "success",
            "message": "List of deliveries retrieved successfully",
            "deliveries": json_deliveries
        }
    ), 200)


def positions_list(drivers):
    json_drivers_info = [
        {
            "driver_id": driver.rowid,
            "name": driver.name,
            "latitude": driver.coordinates.latitude,
            "longitude": driver.coordinates.longitude
        }

        for driver in drivers
    ]

    return make_response(jsonify(
        {
            "status": "success",
            "message": "List of orders retrieved successfully",
            "drivers": json_drivers_info
        }
    ), 200)

from flask import Blueprint, request
from flask_jwt_extended import (
    jwt_required, create_access_token, get_jwt_identity
)

from app.driver.helpers import *
from app.helpers import json_response, verify_password

driver = Blueprint('driver', __name__)


@driver.route('/login', methods=['POST'])
def login():
    if not (request.is_json
            and 'login' in request.json
            and 'password' in request.json):
        return json_response(
            "fail",
            "Request contains no JSON body or required field",
            400
        )

    driver = Driver.query.filter(Authdata.login == request.json['login']).one_or_none()

    if not driver:
        return json_response(
            "fail",
            "No driver with such login found",
            400
        )

    if not verify_password(driver.authdata, request.json['password']):
        return json_response(
            "fail",
            "Invalid credentials",
            400
        )

    return jsonify(
        status="success",
        message="Logged in as {}".format(driver.name),
        access_token=create_access_token(identity=driver.authdata.login)
    )


@driver.route('/map', methods=('POST',))
@jwt_required
def save_coordinates():
    driver = Driver.query.filter(Authdata.login == get_jwt_identity()).one_or_none()

    if not driver:
        return json_response(
            "fail",
            "Could not find driver that is logged in",
            500
        )

    if not (request.is_json
            # and 'driver_id' in request.json
            and 'latitude' in request.json
            and 'longitude' in request.json):
        return json_response(
            "fail",
            "Request contains no JSON body or required field",
            400
        )

    if not update_position(driver, request.json["latitude"],
                           request.json["longitude"]):
        return json_response(
            "fail",
            "Database error",
            500
        )

    return json_response(
        "success",
        "Driver's position successfully updated",
        200
    )


@driver.route('/current_delivery', methods=('GET',))
@jwt_required
def current_delivery():
    driver = Driver.query.filter(Authdata.login == get_jwt_identity()).one_or_none()

    if not driver:
        return json_response(
            "fail",
            "Could not find driver that is logged in",
            500
        )

    else:
        delivery = None
        deliveries = Delivery.query.filter_by(driver_id=driver.rowid)

        if not deliveries:
            return json_response(
                "fail",
                "Database error",
                500
            )

        for deliv in deliveries:
            if deliv.status.status == "IN_PROGRESS":
                delivery = deliv

        return fetch_current_delivery(delivery)


@driver.route('/passed_deliveries', methods=('GET',))
@jwt_required
def passed_deliveries():
    driver = Driver.query.filter(Authdata.login == get_jwt_identity()).one_or_none()

    if not driver:
        return json_response(
            "fail",
            "Could not find driver that is logged in",
            500
        )

    else:
        delivery = []
        deliveries = Delivery.query.filter_by(driver_id=driver.rowid)

        if not deliveries:
            return json_response(
                "fail",
                "Database error",
                500
            )

        for deliv in deliveries:
            if deliv.status.status == "SUCCESS":
                delivery.append(deliv)

        return fetch_previous_deliveries(delivery)


@driver.route('/update_status', methods=('POST',))
@jwt_required
def update_status():
    if not (request.is_json
            and 'parcel_id' in request.json
            and 'status' in request.json):
        return json_response(
            "fail",
            "Request contains no JSON body or required field",
            400
        )

    driver = Driver.query.filter(Authdata.login == get_jwt_identity()).one_or_none()

    if not driver:
        return json_response(
            "fail",
            "Could not find driver that is logged in",
            500
        )

    report = "Successfully delivered"

    if request.json['status'] == "Fail":
        if not ("report" in request.json):
            return json_response(
                "fail",
                "Request contains no JSON body or required field",
                400
            )
        else:
            status = "TO_DELIVER"
            report = request.json["report"]

    elif request.json['status'] == "Success":
        status = "SUCCESS"

    else:
        return json_response(
            "fail",
            "Request field status is wrong",
            400
        )

    try:
        parcel_id = int(request.json['parcel_id'])
    except:
        return json_response(
            "fail",
            "Request field parcel_id malformed",
            400
        )

    delivery = Delivery.query.filter_by(driver_id=driver.rowid).filter_by(parcel_id=parcel_id)

    if not delivery:
        return json_response(
            "fail",
            "Delivery with associated driver_id and parcel_id not found",
            400
        )

    if delivery.count() != 1:
        return json_response(
            "fail",
            "Database error",
            500
        )

    if not update_delivery_status(delivery.first(), status,
                                  report):
        return json_response(
            "fail",
            "Database error",
            500
        )

    return json_response(
        "success",
        "Driver's delivery status successfully updated",
        200
    )

@driver.route('/future_deliveries', methods=('GET',))
@jwt_required
def future_deliveries():
    driver = Driver.query.filter(Authdata.login == get_jwt_identity()).one_or_none()

    if not driver:
        return json_response(
            "fail",
            "Could not find driver that is logged in",
            500
        )

    else:
        delivery = []
        deliveries = Delivery.query.filter_by(driver_id=driver.rowid)

        if not deliveries:
            return json_response(
                "fail",
                "Database error",
                500
            )

        for deliv in deliveries:
            if deliv.status.status == "TO_DELIVER":
                delivery.append(deliv)

        return fetch_future_deliveries(delivery, driver.coordinates.longitude, driver.coordinates.latitude)
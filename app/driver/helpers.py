from flask import make_response, jsonify

from app.models import *
from math import sin, cos, sqrt, atan2, radians

def update_position(driver, latitude, longitude):
    driver.coordinates.latitude = latitude
    driver.coordinates.longitude = longitude

    try:
        db.session.commit()
    except:
        return False

    return True


def update_delivery_status(delivery, status, report):
    delivery.status = Status.query.filter_by(status=status).first()
    delivery.report.report = report

    try:
        db.session.commit()
    except:
        return False

    return True


def fetch_current_delivery(delivery):
    if not delivery:
        return make_response(jsonify(
            {
                "status": "success",
                "message": "No current deliveries",
            }
        ), 200)

    json_info = {
        "delivery_id": delivery.rowid,
        "delivery_address": delivery.parcel.delivery_address,
        "pickup_address": delivery.parcel.pickup_address,
        "delivery_coordinates":
            {
                "latitude": delivery.parcel.delivery_coordinates.latitude,
                "longitude": delivery.parcel.delivery_coordinates.longitude
            },
        "time_windows": delivery.parcel.time_windows,
        "client_name": delivery.parcel.client.name,
        "telephone": delivery.parcel.client.phone,
        "email": delivery.parcel.client.email,
        "status": delivery.status.status
    }

    return make_response(jsonify(
        {
            "status": "success",
            "message": "Information about the delivery {} retrieved successfully".format(delivery.rowid),
            "delivery": json_info
        }
    ), 200)

def calculate_distance (lng1, lat1, lng2, lat2):
    R = 6373.0

    lat1 = radians(float(lat1))
    lon1 = radians(float(lng1))
    lat2 = radians(float(lat2))
    lon2 = radians(float(lng2))

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    distance = R * c
    return distance

def fetch_future_deliveries(deliveries, driver_lng, driver_lat):
    if not deliveries:
        return make_response(jsonify(
            {
                "status": "success",
                "message": "No more deliveries",
            }
        ), 200)

    sorted_deliveries = sorted(deliveries,
        key = lambda delivery: calculate_distance(delivery.parcel.delivery_coordinates.longitude,
                                                  delivery.parcel.delivery_coordinates.latitude,
                                                  driver_lng,
                                                  driver_lat))

    json_info = [{
        "delivery_id": delivery.rowid,
        "delivery_address": delivery.parcel.delivery_address,
        "client_name": delivery.parcel.client.name,
        "useful_data": delivery.report.report,
        "status": delivery.status.status,
        "delivery_coordinates":
            {
                "latitude": delivery.parcel.delivery_coordinates.latitude,
                "longitude": delivery.parcel.delivery_coordinates.longitude
            }
    } for delivery in sorted_deliveries]

    return make_response(jsonify(
        {
            "status": "success",
            "message": "Information about the future deliveries retrieved successfully",
            "delivery": json_info
        }
    ), 200)


def fetch_previous_deliveries(deliveries):
    if not deliveries:
        return make_response(jsonify(
            {
                "status": "success",
                "message": "No previous deliveries deliveries",
            }
        ), 200)

    json_info = [{
        "delivery_id": delivery.rowid,
        "delivery_address": delivery.parcel.delivery_address,
        "client_name": delivery.parcel.client.name,
        "useful_data": delivery.report.report,
        "status": delivery.status.status
    } for delivery in deliveries]

    return make_response(jsonify(
        {
            "status": "success",
            "message": "Information about the previous deliveries retrieved successfully",
            "delivery": json_info
        }
    ), 200)

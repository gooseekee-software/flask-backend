from flask import Blueprint, request
from flask_jwt_extended import (
    jwt_required, create_access_token, get_jwt_identity
)
from werkzeug.security import generate_password_hash

from app.customer.helpers import *
from app.helpers import json_response, validate_phone, \
    validate_password, verify_password, validate_email
from app.models import *

customer = Blueprint('customer', __name__)


@customer.route('/register', methods=['POST'])
def register():
    if not (request.is_json
            and 'name' in request.json
            and 'email' in request.json
            and 'phone' in request.json
            and 'password' in request.json):
        return json_response(
            'fail',
            'Request contains no JSON body or required field',
            400
        )

    if not validate_email(request.json['email']):
        return json_response(
            'fail',
            'Specified email address doesn\'t satisfy the requirements',
            400
        )

    if not validate_phone(request.json['phone']):
        return json_response(
            'fail',
            'Specified phone number doesn\'t satisfy the requirements',
            400
        )

    if not validate_password(request.json['password']):
        return json_response(
            'fail',
            'Specified password doesn\'t satisfy the requirements',
            400
        )

    client_role = Role.query.filter_by(name="Customer").first()

    trinity = generate_password_hash(request.json['password'],
                                     method='pbkdf2:sha256',
                                     salt_length=8)
    salt, password_hash = trinity.split('$')[1:]

    authdata = Authdata(login=request.json['email'], authhash=password_hash,
                        salt=salt, role=client_role)
    client = Client(name=request.json['name'], phone=request.json['phone'],
                    email=request.json['email'], authdata=authdata)
    db.session.add(client)

    try:
        db.session.commit()
        return json_response(
            "success",
            "Client successfully registered",
            200
        )

    except:
        return json_response(
            "fail",
            "Database error",
            500
        )


@customer.route('/login', methods=['POST'])
def login():
    if not (request.is_json
            and 'email' in request.json
            and 'password' in request.json):
        return json_response(
            "fail",
            "Request contains no JSON body or required field",
            400
        )

    client = Client.query.filter_by(email=request.json['email']).one_or_none()

    if not client:
        return json_response(
            "fail",
            "No client with such email found",
            400
        )

    if not verify_password(client.authdata, request.json['password']):
        return json_response(
            "fail",
            "Invalid credentials",
            400
        )

    return jsonify(
        status="success",
        message="Logged in as {}".format(client.name),
        access_token=create_access_token(identity=client.email)
    )


@customer.route('/create_order', methods=('POST',))
@jwt_required
def create_order():
    if not (request.is_json
            and 'delivery_address' in request.json
            and 'pickup_address' in request.json
            and 'time_windows' in request.json
            and 'delivery_coordinates' in request.json
            and 'pickup_coordinates' in request.json):
        return json_response(
            "fail",
            "Request contains no JSON body or required field",
            400
        )

    if not ('latitude' in request.json['delivery_coordinates']
            and 'longitude' in request.json['delivery_coordinates']):
        return json_response(
            "fail",
            "Delivery coordinates field malformed",
            400
        )

    client = Client.query.filter_by(email=get_jwt_identity()).one_or_none()

    if not client:
        return json_response(
            "fail",
            "Could not find client that is logged in",
            500
        )

    delivery_coordinates = Coordinates(
        latitude=request.json['delivery_coordinates']['latitude'],
        longitude=request.json['delivery_coordinates']['longitude']
    )
    pickup_coordinates = Coordinates(
        latitude=request.json['pickup_coordinates']['latitude'],
        longitude=request.json['pickup_coordinates']['longitude']
    )
    parcel = Parcel(delivery_address=request.json['delivery_address'],
                    pickup_address=request.json['pickup_address'],
                    time_windows=request.json['time_windows'],
                    client=client,
                    delivery_coordinates=delivery_coordinates,
                    pickup_coordinates=pickup_coordinates)
    db.session.add(parcel)

    try:
        db.session.commit()

        return json_response(
            "success",
            "Order created successfully",
            200
        )

    except:
        return json_response(
            "fail",
            "Database error",
            500
        )


@customer.route('/order_list', methods=('GET',))
@jwt_required
def get_order_list():
    client = Client.query.filter_by(email=get_jwt_identity()).one_or_none()
    if not client:
        return json_response(
            "fail",
            "Unauthorized",
            401
        )

    return jsonify({
        "status": "success",
        "message": "List of orders retrieved successfully",
        "orders": order_list(client.parcels)
    })


@customer.route('/order_info', methods=('GET',))
@jwt_required
def get_order_info():
    if 'parcel_id' in request.args:
        client = Client.query.filter_by(email=get_jwt_identity()).one_or_none()
        if not client:
            return json_response(
                "fail",
                "Unauthorized",
                401
            )

        try:
            parcel_id = int(request.args['parcel_id'])
        except:
            return json_response(
                "fail",
                "Request field parcel_id malformed",
                400
            )

        parcel = Parcel.query.get(parcel_id)

        if not parcel:
            return json_response(
                "fail",
                "Database error",
                500
            )

        if parcel.client.rowid != client.rowid:
            return json_response(
                "fail",
                "Unauthorized",
                401
            )

        return order_info(parcel)

    else:
        return json_response(
            "fail",
            "Request contains no required field: parcel_id",
            400
        )

from flask import make_response, jsonify


def order_list(parcels):
    return [
        {
            "parcel_id": parcel.rowid,
            "status": parcel.delivery.status if parcel.delivery else "PENDING"
        }
        for parcel in parcels
    ]


def order_info(parcel):
    json_info = {
        "parcel_id": parcel.rowid,
        "delivery_address": parcel.delivery_address,
        "pickup_address": parcel.pickup_address,
        "delivery_coordinates":
            {
                "latitude": parcel.delivery_coordinates.latitude,
                "longitude": parcel.delivery_coordinates.longitude
            },
        "status": parcel.delivery.status if parcel.delivery else "PENDING"
    }

    return make_response(jsonify(
        {
            "status": "success",
            "message": "Information about the order {} retrieved successfully".format(parcel.rowid),
            "order": json_info
        }
    ), 200)

import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_jwt_extended import JWTManager


app = Flask("Backend Server", static_folder=None)


jwt = JWTManager(app)


app_settings = os.getenv(
    'APP_SETTINGS',
    'app.config.DevelopmentConfig'
)
app.config.from_object(app_settings)


db = SQLAlchemy(app)
migrate = Migrate(app, db)


from app import views


from app.ceo.views import ceo
from app.customer.views import customer
from app.driver.views import driver
from app.operator.views import operator


app.register_blueprint(ceo, url_prefix="/ceo")
app.register_blueprint(customer, url_prefix="/customer")
app.register_blueprint(driver, url_prefix="/driver")
app.register_blueprint(operator, url_prefix="/operator")

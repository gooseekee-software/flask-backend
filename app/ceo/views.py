from flask import Blueprint, request

from app.helpers import json_response
from app.models import Delivery, Driver
from app.ceo.helpers import daily_stats_info, positions_list


ceo = Blueprint('ceo', __name__)


@ceo.route('/daily_stats', methods=('GET',))
def daily_stats():
    try:
        deliveries = Delivery.query.all()

    except:
        return json_response(
            "fail",
            "Database error",
            500
        )

    return daily_stats_info(deliveries)


@ceo.route('/map', methods=('GET',))
def get_positions():
    try:
        drivers = Driver.query.all()

    except:
        return json_response(
            "fail",
            "Database error",
            500
        )

    return positions_list(drivers)

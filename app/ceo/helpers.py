from flask import make_response, jsonify


def daily_stats_info(deliveries):
    delivered = sum(order.status.status == 'SUCCESS' for order in deliveries)
    in_progress = sum(order.status.status == 'IN_PROGRESS'
                      for order in deliveries)
    total_amount_to_deliver = sum(order.status.status == 'TO_DELIVER'
                                  for order in deliveries)

    json_info = {
        "delivered": delivered,
        "in_progress": in_progress,
        "estimated": total_amount_to_deliver,
    }

    return make_response(jsonify(
        {
            "status": "success",
            "message": "Stats information retrieved successfully",
            "order": json_info
        }
    ), 200)


def positions_list(drivers):
    json_drivers_info = [
        {
            "driver_id": driver.rowid,
            "name": driver.name,
            "latitude": driver.coordinates.latitude,
            "longitude": driver.coordinates.longitude
        }

        for driver in drivers
    ]

    return make_response(jsonify(
        {
            "status": "success",
            "message": "List of drivers retrieved successfully",
            "drivers": json_drivers_info
        }
    ), 200)

from flask import make_response, jsonify

from app import app, jwt


@jwt.expired_token_loader
def jwt_expired():
    return make_response(jsonify({
        "status": "fail",
        "message": "JWT-token expired"
    }), 401)


@jwt.invalid_token_loader
def jwt_invalid(cause):
    return make_response(jsonify({
        "status": "fail",
        "message": "JWT-token invalid"
    }), 401)


@jwt.unauthorized_loader
def jwt_unauthorized(cause):
    return make_response(jsonify({
        "status": "fail",
        "message": "Unauthorized"
    }), 401)


@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404


@app.route('/favicon.ico')
def favicon():
    return "about:blank"

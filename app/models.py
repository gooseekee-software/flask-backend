import random

from app import db


class Role(db.Model):
    __tablename__ = "t_roles"

    rowid = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(45), nullable=False)
    permissions = db.Column(db.String(8), nullable=False)


class Authdata(db.Model):
    __tablename__ = "t_authdata"

    rowid = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(40), nullable=False, unique=True)
    authhash = db.Column(db.String(64), nullable=False)
    salt = db.Column(db.String(8), nullable=False)
    role_id = db.Column(db.Integer,
                        db.ForeignKey('t_roles.rowid'),
                        nullable=False)

    role = db.relationship('Role', backref=db.backref('authdata'))


class Client(db.Model):
    __tablename__ = "t_clients"

    rowid = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), nullable=False)
    phone = db.Column(db.String(15), nullable=False)
    email = db.Column(db.String(45), nullable=False)
    authdata_id = db.Column(db.Integer,
                            db.ForeignKey('t_authdata.rowid'),
                            nullable=False)

    authdata = db.relationship('Authdata')


class Coordinates(db.Model):
    __tablename__ = "t_coordinates"

    rowid = db.Column(db.Integer, primary_key=True)
    latitude = db.Column(db.String(32), nullable=False)
    longitude = db.Column(db.String(32), nullable=False)


class Driver(db.Model):
    __tablename__ = "t_drivers"

    rowid = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), nullable=False)
    phone = db.Column(db.String(15), nullable=False)
    authdata_id = db.Column(db.Integer,
                            db.ForeignKey('t_authdata.rowid'),
                            nullable=False)
    coordinates_id = db.Column(db.Integer,
                               db.ForeignKey('t_coordinates.rowid'),
                               nullable=False)

    authdata = db.relationship('Authdata')
    coordinates = db.relationship('Coordinates')


class Status(db.Model):
    __tablename__ = "t_statuses"

    rowid = db.Column(db.Integer, primary_key=True)
    status = db.Column(db.String(45), nullable=False)


class Report(db.Model):
    __tablename__ = "t_reports"

    rowid = db.Column(db.Integer, primary_key=True)
    report = db.Column(db.Text, nullable=False)


class Parcel(db.Model):
    __tablename__ = "t_parcels"

    rowid = db.Column(db.Integer, primary_key=True)
    delivery_address = db.Column(db.String(100), nullable=False)
    pickup_address = db.Column(db.String(100), nullable=False)
    time_windows = db.Column(db.String(50), nullable=False)
    client_id = db.Column(db.Integer,
                          db.ForeignKey('t_clients.rowid'),
                          nullable=False)
    delivery_coordinates_id = db.Column(db.Integer,
                                        db.ForeignKey('t_coordinates.rowid'),
                                        nullable=False)
    pickup_coordinates_id = db.Column(db.Integer,
                                      db.ForeignKey('t_coordinates.rowid'),
                                      nullable=False)

    client = db.relationship('Client', backref=db.backref('parcels'))
    delivery_coordinates = db.relationship(
        'Coordinates',
        foreign_keys=[delivery_coordinates_id]
    )
    pickup_coordinates = db.relationship(
        'Coordinates',
        foreign_keys=[pickup_coordinates_id]
    )


class Delivery(db.Model):
    __tablename__ = "t_deliveries"

    rowid = db.Column(db.Integer, primary_key=True)
    driver_id = db.Column(db.Integer,
                          db.ForeignKey('t_drivers.rowid'),
                          nullable=False)
    parcel_id = db.Column(db.Integer,
                          db.ForeignKey('t_parcels.rowid'),
                          nullable=False)
    report_id = db.Column(db.Integer, db.ForeignKey('t_reports.rowid'))
    status_id = db.Column(db.Integer,
                          db.ForeignKey('t_statuses.rowid'),
                          nullable=False)

    driver = db.relationship('Driver', backref=db.backref('deliveries'))
    parcel = db.relationship('Parcel',
                             backref=db.backref('delivery', uselist=False))
    report = db.relationship('Report',
                             backref=db.backref('delivery', uselist=False))
    status = db.relationship('Status', backref=db.backref('deliveries'))

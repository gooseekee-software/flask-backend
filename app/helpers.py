from flask import make_response, jsonify
import re

from werkzeug.security import check_password_hash


def json_response(status, message, status_code):
    return make_response(jsonify({
        "status": status,
        "message": message
    }), status_code)


def validate_email(email):
    regex = '^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$'
    if re.match(regex, email):
        return True
    return False


def validate_phone(phone):
    regex = '\+?\d{11,15}'
    if re.match(regex, phone):
        return True
    return False


def validate_password(password):
    length = re.match('^.{8,32}$', password)
    ascii = re.match('^[\x20-\x7E]+$', password)
    upper = re.match('.*[A-Z].*', password)
    lower = re.match('.*[a-z].*', password)
    number = re.match('.*[0-9].*', password)
    special = re.match('.*[ !"#$%&\'()*+,-./:;<=>?@\][\\\^_`{|}~].*', password)
    if length and ascii and upper and lower and number and special:
        return True
    else:
        return False


def verify_password(authdata, password):
    trinity = '$'.join(['pbkdf2:sha256', authdata.salt, authdata.authhash])
    return check_password_hash(trinity, password)

import click
from flask_migrate import MigrateCommand

from app import app, db
from app.models import *


@app.cli.command()
def init_db():
    db.create_all()

    client_role = Role.query.filter_by(name="Customer").one_or_none()
    driver_role = Role.query.filter_by(name="Driver").one_or_none()
    operator_role = Role.query.filter_by(name="Operator").one_or_none()

    if not client_role:
        client_role = Role(name="Customer", permissions=0)
        db.session.add(client_role)

    if not driver_role:
        driver_role = Role(name="Driver", permissions=0)
        db.session.add(driver_role)

    if not operator_role:
        operator_role = Role(name="Operator", permissions=0)
        db.session.add(operator_role)

    try:
        db.session.commit()
    except:
        click.echo("Failed to create default roles")
        return

    success_status = Status.query.filter_by(status="SUCCESS").one_or_none()
    in_progress_status = (Status.query
                          .filter_by(status="IN_PROGRESS")
                          .one_or_none())
    to_deliver_status = (Status.query
                         .filter_by(status="TO_DELIVER")
                         .one_or_none())

    if not success_status:
        success_status = Status(status="SUCCESS")
        db.session.add(success_status)

    if not in_progress_status:
        in_progress_status = Status(status="IN_PROGRESS")
        db.session.add(in_progress_status)

    if not to_deliver_status:
        to_deliver_status = Status(status="TO_DELIVER")
        db.session.add(to_deliver_status)

    try:
        db.session.commit()
    except:
        click.echo("Failed to create default statuses")
        return

    click.echo("Database initialized")


if __name__ == "__main__":
    app.run()

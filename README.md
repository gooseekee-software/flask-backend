# Flask Backend

This repository holds Backend Server for GooSeeKee team software project
for Software Architecture course at Innopolis University.

## Deployment

Before deployment, make sure you have all prerequisities and dependencies
installed.  Refer to section Requirements.

### Local

The application can be deployed locally in two ways:

1.  Using heroku
2.  Manned launch

If Heroku CLI is installed in your system, just use this:

```bash
heroku local
```

Even if Heroku CLI is not installed in your system, you can launch application
manually by giving Python WSGI app to a WSGI server such as gunicorn:

```bash
gunicorn gaagaa:app
```

### Remote

The application can be and, in fact, is deployed at
[Heroku](https://gaagaa-backend.herokuapp.com/)

## Requirements

The main application requirement is Python >= 3.5; the other requirements,
such as libraries and modules, can be fulfilled using pip >= 9.0.1 via
`requirements.txt` file provided along the distribution.
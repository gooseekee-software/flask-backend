from json import dumps
from werkzeug.security import generate_password_hash

import pytest
from flask import url_for

from app import app, db
from app.models import *


def execute_method(method, *args):
    rv = method(*args)
    return rv.get_json()


def save_coordinates(client, login, password,
                 lat, lng):

    data = client.post(
        '/driver/login',
        content_type='application/json',
        data=dumps({
            "login": login,
            "password": password
        })
    )

    data_json = data.json

    if data_json['status'] == 'fail':
        return data

    return client.post(
        '/driver/map',
        content_type='application/json',
        data=dumps({
            "latitude": lat,
            "longitude": lng
        }),
        headers={
            'Authorization': 'Bearer {}'.format(data_json['access_token'])
        }
    )
def current_delivery(client, login, password):
    data = client.post(
        '/driver/login',
        content_type='application/json',
        data=dumps({
            "login": login,
            "password": password
        })
    )

    data_json = data.json

    if data_json['status'] == 'fail':
        return data

    return client.get(
        '/driver/current_delivery',
        headers ={
        'Authorization': 'Bearer {}'.format(data_json['access_token'])
    }
    )

def previous_delivery(client, login, password):
    data = client.post(
        '/driver/login',
        content_type='application/json',
        data=dumps({
            "login": login,
            "password": password
        })
    )

    data_json = data.json

    if data_json['status'] == 'fail':
        return data

    return client.get(
        '/driver/passed_deliveries',
        headers={
            'Authorization': 'Bearer {}'.format(data_json['access_token'])
        }
    )

def future_deliveries(client, login, password):
    data = client.post(
        '/driver/login',
        content_type='application/json',
        data=dumps({
            "login": login,
            "password": password
        })
    )

    data_json = data.json

    if data_json['status'] == 'fail':
        return data

    return client.get(
        '/driver/future_deliveries',
        headers={
            'Authorization': 'Bearer {}'.format(data_json['access_token'])
        }
    )

def login(client, login, password):
    return client.post(
        '/driver/login',
        content_type = 'application/json',
        data = dumps({
            "login": login,
            "password": password
        })
    )

def update_status(client, login, password, parcel_id, status, report = ""):
    data = client.post(
        '/driver/login',
        content_type='application/json',
        data=dumps({
            "login": login,
            "password": password
        })
    )

    data_json = data.json

    if data_json['status'] == 'fail':
        return data

    if report == "":
        return client.post(
        'driver/update_status',
        content_type='application/json',
        data=dumps({
            "parcel_id": parcel_id,
            "status": status}),
        headers={
                'Authorization': 'Bearer {}'.format(data_json['access_token'])
            }
        )

    else:
        return client.post(
            'driver/update_status',
            content_type='application/json',
            data=dumps({
                # "driver_id": driver_id,
                "parcel_id": parcel_id,
                "status": status,
                "report": report}),
            headers={
                'Authorization': 'Bearer {}'.format(data_json['access_token'])
            }
        )

@pytest.fixture(scope="module")
def client():
    db.create_all()

    statuses = [
        Status(status="SUCCESS"),
        Status(status="IN_PROGRESS"),
        Status(status="TO_DELIVER")
    ]
    db.session.add_all(statuses)

    roles = [
        Role(name="Customer", permissions=0),
        Role(name="Driver", permissions=1),
        Role(name="Operator", permissions=2)
    ]
    db.session.add_all(roles)

    trinity = generate_password_hash("123456AW!",
                                     method='pbkdf2:sha256',
                                     salt_length=8)
    salt, password_hash = trinity.split('$')[1:]

    authdata = [
        Authdata(login="Customer #1", authhash="", salt="", role=roles[0]),
        Authdata(login="Driver #1", authhash=password_hash, salt=salt, role=roles[1]),
        Authdata(login="Operator #1", authhash="", salt="", role=roles[2])
    ]
    db.session.add_all(authdata)

    coordinates = [
        Coordinates(latitude="52.345678", longitude="38.345678"),
        Coordinates(latitude="12.345678", longitude="12.345678"),
        Coordinates(latitude="12.345678", longitude="12.345678"),
        Coordinates(latitude="55.345678", longitude="33.345678"),
        Coordinates(latitude="12.345678", longitude="12.345678")
    ]
    db.session.add_all(coordinates)

    customer = Client(name="Customer 1", phone="+2134567890",
                      email="customer.1@cust.org", authdata=authdata[0])
    db.session.add(customer)


    driver = Driver(name="Driver 1", phone="+1234567890",
                    authdata=authdata[1], coordinates=coordinates[0])

    db.session.add(driver)

    parcels = [
        Parcel(delivery_address="1", pickup_address="2", time_windows="3",
               client=customer, delivery_coordinates=coordinates[1]),
        Parcel(delivery_address="4", pickup_address="5", time_windows="6",
               client=customer, delivery_coordinates=coordinates[2]),
        Parcel(delivery_address="7", pickup_address="8", time_windows="9",
               client=customer, delivery_coordinates=coordinates[3]),
        Parcel(delivery_address="0", pickup_address="0", time_windows="0",
               client=customer, delivery_coordinates=coordinates[4])
    ]
    db.session.add_all(parcels)

    reports = [
        Report(report="0"),
        Report(report="1"),
        Report(report="2"),
        Report(report="3")
    ]

    deliveries = [
        Delivery(driver=driver, parcel=parcels[0],
                 report=reports[0], status=statuses[0]),
        Delivery(driver=driver, parcel=parcels[1],
                 report=reports[1], status=statuses[0]),
        Delivery(driver=driver, parcel=parcels[2],
                 report=reports[2], status=statuses[2]),
        Delivery(driver=driver, parcel=parcels[3],
                 report=reports[0], status=statuses[2])
    ]
    db.session.add_all(deliveries)

    db.session.commit()

    yield app.test_client()

    db.drop_all()


def test_save_coordinates(client):
    json_answer = execute_method(save_coordinates, client, "Driver #1", "123456AW!", 1.88, 3.456)
    assert json_answer['status'] == "success"


def test_current_delivery(client ):
    json_answer = execute_method(current_delivery, client, "Driver #1", "123456AW!")
    assert json_answer['status'] == "success"
    if 'delivery' in json_answer:
        assert 'time_windows' in json_answer['delivery']
        assert 'latitude' in json_answer['delivery']['delivery_coordinates']
        assert 'longitude' in json_answer['delivery']['delivery_coordinates']

def test_previous_delivery(client):
    json_answer = execute_method(previous_delivery, client, "Driver #1", "123456AW!")
    assert json_answer['status'] == "success"
    if 'delivery' in json_answer:
        assert len(json_answer["delivery"]) == 2

def test_future_deliveries(client):
    json_answer = execute_method(future_deliveries, client, "Driver #1", "123456AW!")
    assert json_answer['status'] == "success"
    print(json_answer)
    if 'delivery' in json_answer:
        assert len(json_answer["delivery"]) == 2

def test_update_status(client):
    json_answer = execute_method(update_status, client, "Driver #1", "123456AW!", 1, "Success")
    assert json_answer['status'] == 'success'

    json_answer = execute_method(update_status, client,  "Driver #1", "123456AW!",1, "Fail", "balabalalj")
    assert json_answer['status'] == 'success'

def test_login(client):
    json_answer = execute_method(login, client, "Driver #1", "123456AW!")
    assert json_answer['status'] == 'success'

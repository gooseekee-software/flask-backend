from json import dumps

import pytest
from flask import url_for

from app import app, db
from app.models import *


def execute_method(method, *args):
    rv = method(*args)
    return rv.get_json()


def list_orders(client):
    return client.get(
        "/operator/list_orders"
    )


@pytest.fixture(scope="module")
def client():
    db.create_all()

    yield app.test_client()

    db.drop_all()


def test_list_orders(client):
    pass

from json import dumps

import pytest
from flask import url_for

from app import app, db
from app.models import *


def execute_method(method, *args):
    rv = method(*args)
    return rv.get_json()


def create_order(client, customer_name,
                 source_address, destination_address):
    return client.post(
        '/customer/create_order',
        content_type='application/json',
        data=dumps({
            "customer_name": customer_name,
            "source_address": source_address,
            "destination_address": destination_address
        })
    )


def get_order_info(client):
    return client.get(
        '/customer/order_info?order_id=1'
    )


@pytest.fixture(scope="module")
def client():
    db.create_all()

    yield app.test_client()

    db.drop_all()


def test_create_order(client):
    pass


def test_get_order_info(client):
    pass

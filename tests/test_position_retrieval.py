from json import dumps

import pytest
from flask import url_for

from app import app, db
from app.models import Driver


def execute_method(method, *args):
    rv = method(*args)
    return rv.get_json()


def get_coordinates_operator(client):
    return client.get("/operator/map")


def get_coordinates(client, id,
                 lat, lng):

    client.post(
        '/driver/map',
        content_type='application/json',
        data=dumps({
            "id": id,
            "lat": lat,
            "lng": lng
        })
    )

    return client.get(
        "/ceo/map"
    )


@pytest.fixture(scope="module")
def client():
    db.create_all()

    yield app.test_client()

    db.drop_all()


def test_get_coordinates(client):
    pass

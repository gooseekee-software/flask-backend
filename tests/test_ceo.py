from json import dumps

import pytest
from flask import url_for

from app import app, db
from app.models import *


def execute_method(method, *args):
    rv = method(*args)
    return rv.get_json()


def daily_stats(client):
    return client.get(
        "/ceo/daily_stats"
    )


@pytest.fixture(scope="module")
def client():
    db.create_all()

    statuses = [
        Status(status="SUCCESS"),
        Status(status="IN_PROGRESS"),
        Status(status="TO_DELIVER")
    ]
    db.session.add_all(statuses)

    roles = [
        Role(name="Customer", permissions=0),
        Role(name="Driver", permissions=1),
        Role(name="Operator", permissions=2)
    ]
    db.session.add_all(roles)

    authdata = [
        Authdata(login="Customer #1", authhash="", salt="", role=roles[0]),
        Authdata(login="Driver #1", authhash="", salt="", role=roles[1]),
        Authdata(login="Operator #1", authhash="", salt="", role=roles[2])
    ]
    db.session.add_all(authdata)

    coordinates = [
        Coordinates(latitude="12.345678", longitude="12.345678"),
        Coordinates(latitude="12.345678", longitude="12.345678"),
        Coordinates(latitude="12.345678", longitude="12.345678"),
        Coordinates(latitude="12.345678", longitude="12.345678"),
        Coordinates(latitude="12.345678", longitude="12.345678"),
        Coordinates(latitude="12.345678", longitude="12.345678"),
        Coordinates(latitude="12.345678", longitude="12.345678"),
        Coordinates(latitude="12.345678", longitude="12.345678"),
        Coordinates(latitude="12.345678", longitude="12.345678"),
        Coordinates(latitude="12.345678", longitude="12.345678"),
    ]
    db.session.add_all(coordinates)

    customer = Client(name="Customer 1", phone="+2134567890",
                      email="customer.1@cust.org", authdata=authdata[0])
    db.session.add(customer)

    driver = Driver(name="Driver 1", phone="+1234567890",
                    authdata=authdata[1], coordinates=coordinates[0])
    db.session.add(driver)

    parcels = [
        Parcel(delivery_address="1", pickup_address="2", time_windows="3",
               client=customer, delivery_coordinates=coordinates[1],
               pickup_coordinates=coordinates[5]),
        Parcel(delivery_address="4", pickup_address="5", time_windows="6",
               client=customer, delivery_coordinates=coordinates[2],
               pickup_coordinates=coordinates[6]),
        Parcel(delivery_address="7", pickup_address="8", time_windows="9",
               client=customer, delivery_coordinates=coordinates[3],
               pickup_coordinates=coordinates[7]),
        Parcel(delivery_address="0", pickup_address="0", time_windows="0",
               client=customer, delivery_coordinates=coordinates[4],
               pickup_coordinates=coordinates[8])
    ]
    db.session.add_all(parcels)

    reports = [
        Report(report="0"),
        Report(report="1"),
        Report(report="2"),
        Report(report="3")
    ]

    deliveries = [
        Delivery(driver=driver, parcel=parcels[0],
                 report=reports[0], status=statuses[0]),
        Delivery(driver=driver, parcel=parcels[1],
                 report=reports[1], status=statuses[0]),
        Delivery(driver=driver, parcel=parcels[2],
                 report=reports[2], status=statuses[1]),
        Delivery(driver=driver, parcel=parcels[3],
                 report=reports[0], status=statuses[2])
    ]
    db.session.add_all(deliveries)

    db.session.commit()

    yield app.test_client()

    db.drop_all()


def test_daily_stats(client):
    # List all orders in database
    json_answer = execute_method(daily_stats, client)
    assert json_answer['status'] == "success"
    assert 'delivered' in json_answer ['order']
    assert 'in_progress' in json_answer['order']
    assert 'estimated' in json_answer ['order']
    assert json_answer['order']['delivered'] == 2
    assert json_answer['order']['in_progress'] == 1
    assert json_answer['order']['estimated'] == 1
